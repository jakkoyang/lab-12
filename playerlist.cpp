#include <string>
#include <iostream>

using namespace std;

class FootballPlayer {
//change!
private:
    string name;

public:

    FootballPlayer(const string &newName) {
        name = newName;
    }

    string getName() const {
        return name;
    }

};

class FootballPlayerList {

private:
    FootballPlayer **list;
    int size;
    int maxSize;

public:

    FootballPlayerList() : FootballPlayerList(100) {}

    FootballPlayerList(int newMaxSize) {
        maxSize = newMaxSize;
        list = new FootballPlayer*[maxSize];
        size = 0;
    }

    ~FootballPlayerList() {
        for (int i = 0; i < size; i++) {
            delete list[i];
        }
        delete [] list;
    }

    // FootballPlayerList will take ownership of the pointer and delete it
    // on destruction.
    void add(FootballPlayer *player) {
        if (size == maxSize) {
            return;
        }

        list[size++] = player;
    }

    FootballPlayer *getElement(int index) const {
        if (index >= size) {
            return nullptr;
        }
        return list[index];
    }
    
    int getSize() const {
        return size;
    }

};

int main()
{
    FootballPlayerList players;

    players.add(new FootballPlayer("Mahomes"));
    players.add(new FootballPlayer("Kelce"));
    players.add(new FootballPlayer("Pacheco"));

    for (int i = 0; i < players.getSize(); i++) {
        cout << players.getElement(i)->getName() << endl;
    }
        
    return 0;
}
